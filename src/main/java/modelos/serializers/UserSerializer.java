package modelos.serializers;

import java.lang.reflect.Type;

import modelos.User;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UserSerializer implements JsonSerializer<User> {

	@Override
	public JsonElement serialize(User user, Type type,
			JsonSerializationContext context) {
		JsonObject object = new GsonBuilder().create().toJsonTree(user)
				.getAsJsonObject();
		object.remove("password");
		return object;
	}

}
