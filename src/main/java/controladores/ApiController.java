package controladores;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public abstract class ApiController {
	protected JsonObject parseJson(String rawString) {
		JsonParser parser = new JsonParser();
		return (JsonObject) parser.parse(rawString);
	}
}
