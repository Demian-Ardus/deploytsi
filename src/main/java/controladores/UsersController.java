package controladores;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modelos.User;
import modelos.serializers.UserSerializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import daos.implementaciones.UsersCollection;
import daos.interfaces.UserDAO;

@ManagedBean
@ApplicationScoped
@Path("/usuarios")
public class UsersController extends ApiController {

	@GET
	public String index() {
		return "hola";
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String create(String rawData) {
		JsonObject data = parseJson(rawData);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		User user = gson.fromJson(data.get("user"), User.class);

		UserDAO users = new UsersCollection();
		users.save(user);

		return gsonBuilder
				.registerTypeAdapter(User.class, new UserSerializer()).create()
				.toJson(user);
	}
}
