package daos.interfaces;

import java.util.Collection;

public interface GenericDAO<Klass> {
	public Collection<Klass> getAll();

	public Klass getById(String id);

	public boolean destroy(Klass element);

	public boolean save(Klass element);
}
