package daos.interfaces;

import modelos.User;

public interface UserDAO extends GenericDAO<User> {
	public User getByLogin(String userName, String password);
}
