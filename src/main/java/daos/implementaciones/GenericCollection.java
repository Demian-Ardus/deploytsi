package daos.implementaciones;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import daos.interfaces.GenericDAO;

public abstract class GenericCollection<Klass> implements GenericDAO<Klass> {

	private EntityManager entityManager;

	public GenericCollection() {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("ProyectoJSF_JPA");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public Collection<Klass> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Klass getById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean destroy(Klass element) {
		entityManager.getTransaction().begin();
		entityManager.persist(element);
		entityManager.getTransaction().commit();

		return true;
	}

	public boolean save(Klass element) {
		entityManager.getTransaction().begin();
		entityManager.persist(element);
		entityManager.getTransaction().commit();

		return true;
	}

}
